from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url
from django.contrib import admin
from klinic import settings

urlpatterns = patterns('',
    url(r'^home$', 'patient.views.home', name='home'),
    url(r'^create_patient$', 'patient.views.create_patient', name='create_patient'),
    url(r'^self_create$', 'patient.views.self_create', name='self_create'),
    url(r'^delete_patient$', 'patient.views.delete_patient', name='delete_patient'),
    url(r'^modify_patient/(?P<id>\d+)$', 'patient.views.modify_patient', name='modify_patient'),
    url(r'^view_patient/(?P<id>\d+)$', 'patient.views.view_patient', name='view_patient'),
    url(r'^search$', 'patient.views.search', name='search'),
    url(r'^add_visit$', 'patient.views.add_visit', name='add_visit'),
    url(r'^delete_visit$', 'patient.views.delete_visit', name='delete_visit'),
    url(r'^modify_visit$', 'patient.views.modify_visit', name='modify_visit'),
    url(r'^add_pictures$', 'patient.views.add_pictures', name='add_pictures'),
    url(r'^delete_pictures$', 'patient.views.delete_pictures', name='delete_pictures'),
    url(r'^new_patients$', 'patient.views.new_patients', name='new_patients'),
    url(r'^save_called$', 'patient.views.save_called', name='save_called'),
    url(r'^new_p_count$', 'patient.views.ajax_get_new_patients_count', name='new_p_count'),
    url(r'^thankyou$', TemplateView.as_view(template_name="thankyou.html"))

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
