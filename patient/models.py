import glob
import os
import random
import string
from datetime import datetime
from django.db import models
from django.db.models import Q
from django.db.models import Sum
from klinic import settings
from easy_thumbnails.models import Source, Thumbnail


def gen_img_name(n):
    return ''.join(random.SystemRandom().choice(
        string.lowercase + string.digits) for _ in xrange(n))

def get_image_path(instance, filename):
    return settings.PATIENTS_IMAGES_FOLDER + '%s.jpg' % (gen_img_name(32))


class Patient(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=128)
    sex = models.CharField(max_length=1)
    birth_date = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=512, null=True, blank=True)
    email = models.CharField(max_length=128, null=True, blank=True)
    mobile_num = models.CharField(max_length=16)
    home_num = models.CharField(max_length=16, null=True, blank=True)
    medical_history = models.TextField(null=True, blank=True)
    dental_history = models.TextField(null=True, blank=True)
    is_self_registered = models.BooleanField(default=False)
    image1 = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)
    image2 = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)    
    image3 = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)    
    image4 = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)    
    image5 = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)

    def __unicode__(self):
        return "ID: %s, Name: %s, Mobile: %s, email: %s" % \
                (self.pk, self.name, self.mobile_num, self.email)

    @staticmethod
    def delete_patient(id):
        patient = Patient.objects.filter(pk=id)

        if not patient:
            return False

        Visit.delete_visits(patient)
        patient.delete()

        return True

    @staticmethod
    def get_patient(id):
        return Patient.objects.get(pk=id)

    @staticmethod
    def search(params):
        "Searches for a patient by one field at a time"
        name = ''
        mobile_num = ''
        home_num = ''

        if params['name']:
            name = '%'+ params['name'] +'%'
        if params['mobile_num']:
            mobile_num = '%'+ params['mobile_num'] +'%'
        # if params['home_num']:
        #     home_num = '%'+ params['home_num'] +'%'

        results = []

        query = '''SELECT * FROM patient_patient
                   WHERE (name LIKE %s COLLATE nocase
                   OR id = %s
                   OR mobile_num LIKE %s)
                   AND is_self_registered = 0'''
        args = [name, params['pk'], mobile_num]

        # home_num needs a fix.

        for result in Patient.objects.raw(query, args):
            results.append(result)
        return results

    @staticmethod
    def get_patient_list():
        return Patient.objects.all().order_by('-pk').filter(is_self_registered=False)
    
    @staticmethod
    def get_new_patients_list():
        return Patient.objects.all().order_by('-pk')\
                      .filter(is_self_registered=True)
    
    @staticmethod
    def get_new_patients_count():
        return Patient.objects.all().order_by('-pk')\
                      .filter(is_self_registered=True).count()

    @staticmethod
    def save_called_patients(calledPatients):
        Patient.objects.filter(pk__in=calledPatients)\
                       .update(is_self_registered=False)


    @staticmethod
    def get_image_count(id):
        '''Returns a list of empty image fields'''

        patient = vars(Patient.objects.get(pk=id))
        imageFieldsList = ['image1', 'image2', 'image3', 'image4', 'image5']
        emptyImagesList = []
        for imageIndex in range(len(imageFieldsList)):
            if not patient[imageFieldsList[imageIndex]]:
                emptyImagesList.append(imageFieldsList[imageIndex])
        return emptyImagesList

    @staticmethod
    def delete_pictures(id, imagelist):
        p = Patient.objects.get(pk=id)

        for image in imagelist:
            obj = getattr(p, image)
            obj.delete()

        # Clean all thumbs regardless
        for image in glob.glob(settings.MEDIA_ROOT +
                               settings.PATIENTS_IMAGES_FOLDER +
                               '/*_q85_*'):
            os.remove(image)

    @staticmethod
    def get_age(id):
        p = Patient.objects.get(pk=id)

        if p.birth_date:
            return datetime.now().year - p.birth_date.year


class Visit(models.Model):
    uid = models.ForeignKey(Patient)
    complaint = models.TextField()
    cost = models.IntegerField()
    paid = models.IntegerField()
    remarks = models.TextField()
    created_at = models.DateTimeField()

    def __unicode__(self):
        return "Visit ID: %s, owner: %s" % \
                (self.pk, self.uid)

    @staticmethod
    def add_visit(params):
        q = Visit(uid=params['uid'],
                  complaint=params['complaint'],
                  cost=params['cost'],
                  paid=params['paid'],
                  remarks=params['remarks'],
                  created_at=params['created_at']
            )
        q.save()

        if q.pk is not None:
            return q.pk

        return False

    @staticmethod
    def save_visit_edit(params):
        visit = Visit.objects.filter(pk=params['visit_id'])

        if not visit:
            return False

        visit.update(cost=params['cost'],
                     paid=params['paid'],
                     complaint=params['complaint'],
                     remarks=params['remarks'],
                     created_at=params['created_at']
             )
        return True

    @staticmethod
    def delete_visit(visit_id):
        visit = Visit.objects.filter(pk=visit_id)

        if not visit:
            return False

        visit.delete()
        return True

    @staticmethod
    def get_visits(patient_obj):
        return Visit.objects.filter(uid=patient_obj)

    @staticmethod
    def delete_visits(patient_obj):
        Visit.objects.filter(pk=patient_obj).delete()

    @staticmethod
    def get_total(patient_obj, field):
        "Returns the sum of a given db field"
        result = Visit.objects.filter(uid=patient_obj).aggregate(Sum(field))
        field_key = field + '__sum'
        if result[field_key] is None:
            result[field_key] = 0
            return result
        return Visit.objects.filter(uid=patient_obj).aggregate(Sum(field))
