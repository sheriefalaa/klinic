import json
from django.http import HttpResponse
from django.forms.models import modelform_factory
from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from patient.models import Patient, Visit
from patient.forms import *


@login_required
def home(request):
    return render(request, 'patient/templates/home.html')

@login_required
def create_patient(request):
    if 'create_patient' in request.POST:
        form = PatientForm(request.POST, request.FILES)

        if form.is_valid():
            patient = form.save()
            return redirect('/view_patient/'+str(patient.pk))
        else:
            params = {
                'errors': form.errors,
                'supplied_data': request.POST
            }
            return render(request, 'patient/templates/createpatient.html',
                          params)
    return render(request, 'patient/templates/createpatient.html')


def self_create(request):
    form = SelfCreateForm()
    params = {'form': form}

    if 'self_create' in request.POST:
        form = SelfCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/thankyou')
        else:
            params.update({
                'errors': form.errors,
                'supplied_data': request.POST
            })

    return render(request, 'patient/templates/selfcreate.html', params)

@login_required
def new_patients(request):
    params = {
        'patients': Patient.get_new_patients_list()
    }
    return render(request, 'patient/templates/newpatients.html', params)

@login_required
def save_called(request):
    if 'save_called' in request.POST:
        Patient.save_called_patients(request.POST.getlist('called_patients'))
    return redirect('/new_patients')

@login_required
def ajax_get_new_patients_count(request):
    response = {'patient_count': Patient.get_new_patients_count()}
    return HttpResponse(json.dumps(response),
                        content_type="application/json")

@login_required
def view_patient(request, id):
    params = {}
    params['patient'] = get_object_or_404(Patient, pk=id)
    params['visits'] = Visit.get_visits(params['patient'])
    params['emptyImageSlots'] = Patient.get_image_count(id)
    params.update({
        'total_cost': Visit.get_total(params['patient'], 'cost'),
        'total_paid': Visit.get_total(params['patient'], 'paid'),
        'age': Patient.get_age(id) or "",
        })
    params.update({
        'unpaid': params['total_cost']['cost__sum'] - params['total_paid']['paid__sum'],
        })
    return render(request, 'patient/templates/viewpatient.html', params)

@login_required
def modify_patient(request, id):
    patient = Patient.objects.get(pk=id)
    form = PatientForm(request.POST, instance=patient)

    if form.is_valid():
        form.save()
        messages.add_message(request,
                    messages.INFO,
                    'Successfully saved your modifications.')
    else:
        messages.add_message(request,
                            messages.INFO,
                            'Could not save your modifications.')

    return redirect(request.META['HTTP_REFERER'])

@login_required
def add_pictures(request):
    patient = Patient.objects.get(pk=request.POST['patient_id'])

    # Really bad and should be improved.
    if 'image1' in request.FILES:
        patient.image1 = request.FILES['image1']
    if 'image2' in request.FILES:
        patient.image2 = request.FILES['image2']
    if 'image3' in request.FILES:
        patient.image3 = request.FILES['image3']
    if 'image4' in request.FILES:
        patient.image4 = request.FILES['image4']
    if 'image5' in request.FILES:
        patient.image5 = request.FILES['image5']

    patient.save()
    return redirect(request.META['HTTP_REFERER'])

@login_required
def delete_pictures(request):
    imagelist = ['image1', 'image2', 'image3', 'image4', 'image5']
    checkedImagelist = []

    for image in imagelist:
        if image in request.POST:
            checkedImagelist.append(image)
    Patient.delete_pictures(request.POST.get('patient_id'),
                            checkedImagelist)
    return redirect(request.META['HTTP_REFERER'])


@user_passes_test(lambda u: u.is_superuser)
@login_required
def delete_patient(request):
    if Patient.delete_patient(request.GET.get('id')):
        messages.add_message(request,
                            messages.INFO,
                            'Successful delete operation.')
    else:
        messages.add_message(request,
                            messages.INFO,
                            'Could not delete patient.')

    return redirect('/search')

@login_required
def add_visit(request):
    params = {
        'uid': get_object_or_404(Patient, pk=request.POST['uid']),
        'cost': request.POST['cost'],
        'paid': request.POST['paid'],
        'complaint': request.POST['complaint'],
        'remarks': request.POST['remarks'],
        'created_at': request.POST['created_at']
    }

    visit_id = Visit.add_visit(params)

    if visit_id:
        messages.add_message(request,
                            messages.INFO,
                            'Successfully added visit.')
    else:
        messages.add_message(request,
                            messages.INFO,
                            'Could not add visit.')

    return redirect(request.META['HTTP_REFERER'])


@login_required
def modify_visit(request):
    params = {
        'visit_id': request.POST.get('visit_id'),
        'cost': request.POST.get('edit_cost'),
        'paid': request.POST.get('edit_paid'),
        'complaint': request.POST.get('edit_complaint'),
        'remarks': request.POST.get('edit_remarks'),
        'created_at': request.POST.get('edit_created_at')
    }
    if Visit.save_visit_edit(params):
        messages.add_message(request,
                            messages.INFO,
                            'Successfully saved your modifications.')
    else:
        messages.add_message(request,
                            messages.INFO,
                            'Could not save your modifications.')

    return redirect(request.META['HTTP_REFERER'])

@user_passes_test(lambda u: u.is_superuser)
@login_required
def delete_visit(request):
    if Visit.delete_visit(request.GET['id']):
        messages.add_message(request,
                            messages.INFO,
                            'Successful delete operation.')
    else:
        messages.add_message(request,
                            messages.INFO,
                            'Could not delete patient.')

    return redirect(request.META['HTTP_REFERER'])

@login_required
def search(request):
    limit = 10
    params = {}

    if 'search' in request.GET:
        params.update({
            'name': request.GET['name'],
            'mobile_num': request.GET['mobile_num'],
            'home_num': request.GET['home_num'],
            'pk': request.GET.get('patient_id')
        })

        patients_list = Patient.search(params)

        # Responsible for deciding which urls to use in pagination.
        search_mode = True
    else:
        # No input at all; display all records.
        patients_list = Patient.get_patient_list()
        search_mode = False

    # Pagination
    paginator = Paginator(patients_list, limit)
    page = request.GET.get('page')

    try:
        patients = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        patients = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        patients = paginator.page(paginator.num_pages)

    params.update({
        'patients' : patients,
        'pages': range(1, paginator.num_pages + 1),
        'search_mode': search_mode
    })

    return render(request, 'patient/templates/search.html', params)
