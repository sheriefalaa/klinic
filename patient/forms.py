from django import forms
from django.forms import ModelForm
from django.db import models
from patient.models import Patient
from captcha.fields import CaptchaField, CaptchaTextInput


class PatientForm(ModelForm):
    class Meta:
        model = Patient
        fields = ['name', 'sex', 'mobile_num', 'email', 'birth_date', 'address', 'home_num', 'medical_history', 'dental_history']

class SelfCreateForm(ModelForm):
    captcha = CaptchaField(widget=CaptchaTextInput(attrs={'class': 'form-control', 'style': 'width:80px; display: inline;'}))
    class Meta:
        model = Patient
        fields = ['name', 'sex', 'mobile_num', 'email', 'birth_date', 'address', 'home_num', 'medical_history', 'dental_history', 'is_self_registered']
