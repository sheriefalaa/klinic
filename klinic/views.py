from django.contrib.auth.views import login
from django.shortcuts import redirect


def custom_login(request):
    '''
    Extends django's built-in login view to support redirecting already
    logged in users so that they can't view login.html if logged in.
    '''
    if request.user.is_authenticated():
        return redirect('/home')

    return login(request, 'login.html', 'login')