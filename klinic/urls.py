from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

	# Landing page
	url(r'^$', 'klinic.views.custom_login'),

    # Authentication urls (Login Logout)
    url(r'^login$', 'klinic.views.custom_login'),
    url(r'^logout$', 'django.contrib.auth.views.logout_then_login'),

    # Main page, contains statistics.
    # url(r'^home$', 'klinic.views.home', name='home'),

    # Patient app urls
    url(r'', include('patient.urls', 'patient')),
)

# django-simple-captcha
urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)