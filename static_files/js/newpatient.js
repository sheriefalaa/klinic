/* Responsible for getting newly registered patients count who
   registered by themselves from /self_create
*/

var interval;

$(document).ready(function() {
  get_newly_registered_patients_count()
  interval = window.setInterval(get_newly_registered_patients_count, 25000);
});

function get_newly_registered_patients_count() {
  $.ajax({
    type: "POST",
    url: "/new_p_count",
    error: function(){
      alert('Could not get patients new count');
      clearInterval(interval);
    },
    success: function(data){
      $('#p_count').text(data['patient_count']);
    },
    headers: {'X-CSRFToken': getCookie('csrftoken')}
  });
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}