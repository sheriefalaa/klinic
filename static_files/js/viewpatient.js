$(document).ready(function() {
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if(idFor.is(':visible'))
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
            }
            else
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();

    // Event listener for edit
    $(document).on('click', '[name=edit_visit]', function() {
      id = $(this).attr("id").split('-')[2];
      visit_data = getVisitData(id);
      populateVisitModal(visit_data, id);
    });

    $(document).on('click', '[name=complaint]', function () {
      msg = $('#complaint-' + $(this).attr('id').split('-')[1]).val();
      raiseAlert(msg);
    });

    $(document).on('click', '[name=remarks]', function () {
      var msg = $('#remarks-' + $(this).attr('id').split('-')[1]).val();
      raiseAlert(msg);
    });

    $(document).on('click', '[name=delete_images_button]', function () {
      $('#delete_images_form').submit();
    });
});


function getVisitData(id) {
  fields = ['date', 'cost', 'paid', 'complaint', 'remarks'];
  data = [];

  for (i = 0; i < fields.length; i++) {
    if (fields[i] === 'complaint' || fields[i] === 'remarks') {
      data.push($('#'+fields[i]+'-'+id).val());
    } else {
      data.push($('#visit-'+id).children()[i].attributes[fields[i]].value);
    }
  }
  return data;
}

function populateVisitModal(data, id) {
  fields = ['edit_created_at', 'edit_cost', 'edit_paid', 'edit_complaint', 'edit_remarks'];
  for (i = 0; i < fields.length; i++) {
    $('[name='+fields[i]+']').val(data[i]);
    console.log(data[i]);
  }
  $('[name=visit_id]').val(id);
}

function raiseAlert(msg) {
  $('#Alert').modal('show');
  $("#alert-text").html(msg);
}